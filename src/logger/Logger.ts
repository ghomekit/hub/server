import chalk from 'chalk'
import util from 'util'

export const enum LogLevel {
  SUCCESS = 'success',
  INFO = 'info',
  WARN = 'warn',
  ERROR = 'error',
  DEBUG = 'debug',
}

export default class Logger {
  public static readonly internal = new Logger('gHome');
  private static debug: boolean = false
  private static cache: { [key: string]: Logger } = {}

  private prefix?: string = ''

  constructor (prefix?: string) {
    this.prefix = prefix
  }

  static withPrefix (prefix: string): Logger {
    const cachedLogger = Logger.cache[prefix]

    if (cachedLogger) {
      return cachedLogger
    }

    const logger = new Logger(prefix)
    Logger.cache[prefix] = logger

    return logger
  }

  public static setDebugEnabled (enabled = true): void {
    Logger.debug = enabled
  }

  public success (message: string, ...parameters: any[]): void {
    this.log(LogLevel.SUCCESS, message, ...parameters)
  }

  public info (message: string, ...parameters: any[]): void {
    this.log(LogLevel.INFO, message, ...parameters)
  }

  public warn (message: string, ...parameters: any[]): void {
    this.log(LogLevel.WARN, message, ...parameters)
  }

  public error (message: string, ...parameters: any[]): void {
    this.log(LogLevel.ERROR, message, ...parameters)
  }

  public debug (message: string, ...parameters: any[]): void {
    this.log(LogLevel.DEBUG, message, ...parameters)
  }

  public log (level: LogLevel, message: string, ...parameters: any[]): void {
    if (level === LogLevel.DEBUG && !Logger.debug) {
      return
    }

    message = util.format(message, ...parameters)

    let loggingFunction = console.log

    switch (level) {
      case LogLevel.SUCCESS:
        message = chalk.green(message)
        break
      case LogLevel.WARN:
        message = chalk.yellow(message)
        loggingFunction = console.error
        break
      case LogLevel.ERROR:
        message = chalk.red(message)
        loggingFunction = console.error
        break
      case LogLevel.DEBUG:
        message = chalk.gray(message)
        break
    }

    if (this.prefix) {
      message = Logger.getLogPrefix(this.prefix) + ' ' + message
    }

    const date = new Date()
    message = chalk.white(`[${date.toISOString()}] `) + message

    loggingFunction(message)
  }

  public static getLogPrefix (prefix: string): string {
    if (prefix === 'gHome') {
      return chalk.green(`[${prefix}]`)
    }

    return chalk.cyan(`[${prefix}]`)
  }
}
