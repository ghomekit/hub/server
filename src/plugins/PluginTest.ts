import * as os from 'os'
import { spawn } from 'child_process'
import * as fs from 'fs-extra'
import * as path from 'path'
import * as semver from 'semver'
import Logger from '~/logger/Logger'

export default class PluginTest {
  private log: Logger = Logger.internal
  private npm: Array<string> = this.getNpmPath()

  private static readonly PLUGIN_IDENTIFIER_PATTERN =
    /^((@[\w-]*)\/)?(ghome-[\w-]*)$/

  private static readonly NODE_MIN_VERSION = '16.0.0'

  public getNpmPath () {
    if (os.platform() === 'win32') {
      // if running on windows find the full path to npm

      const windowsNpmPath = []

      if (process.env.APPDATA) {
        windowsNpmPath.push(path.join(process.env.APPDATA, 'npm/npm.cmd'))
      }
      if (process.env.ProgramFiles) {
        windowsNpmPath.push(
          path.join(process.env.ProgramFiles, 'nodejs/npm.cmd')
        )
      }
      if (process.env.NVM_SYMLINK || process.env.ProgramFiles) {
        windowsNpmPath.push(
          path.join(
            process.env.NVM_SYMLINK || process.env.ProgramFiles + '/nodejs',
            'npm.cmd'
          )
        )
      }

      if (windowsNpmPath.filter(fs.existsSync).length) {
        return [windowsNpmPath.filter(fs.existsSync)[0]]
      } else {
        this.log.error(
          'ERROR: Cannot find npm binary. You will not be able to manage plugins or update homebridge.'
        )
        this.log.error(
          'ERROR: You might be able to fix this problem by running: npm install -g npm'
        )
      }
    }

    // Linux and macOS don't require the full path to npm / pnpm
    return ['npm']
  }

  public async runNpmCommand (command: Array<string>, cwd: string) {
    let timeoutTimer: NodeJS.Timeout
    command = command.filter(x => x.length)

    // check if there is a command to run
    if (!command.length) {
      this.log.error('No command to run.')
      return
    }

    try {
      await fs.access(path.resolve(cwd, 'node_modules'), fs.constants.W_OK)
    } catch (e) {
      this.log.error(`The user "${os.userInfo().username}" does not have write access to the target directory:`)
      this.log.error(`${path.resolve(cwd, 'node_modules')}`)
    }

    this.log.debug(`Command: ${command.join(' ')}`)

    if (!semver.satisfies(process.version, `>=${PluginTest.NODE_MIN_VERSION}`)) {
      this.log.warn(`Node.js v${PluginTest.NODE_MIN_VERSION} higher is required.`)
      this.log.warn(`You may experience issues while running on Node.js ${process.version}.`)
    }

    // setup the environment for the call
    const env = {}

    Object.assign(env, process.env)
    Object.assign(env, {
      npm_config_global_style: 'true',
      npm_config_unsafe_perm: 'true',
      npm_config_update_notifier: 'false',
      npm_config_prefer_online: 'true',
      npm_config_foreground_scripts: 'true',
      npm_config_loglevel: 'error'
    })

    // set global prefix for unix based systems
    if (command.includes('-g') && path.basename(cwd) === 'lib') {
      cwd = path.dirname(cwd)
      Object.assign(env, {
        npm_config_prefix: cwd
      })
    }

    // on windows we want to ensure the global prefix is the same as the install path
    if (os.platform() === 'win32') {
      Object.assign(env, {
        npm_config_prefix: cwd
      })
    }

    this.log.debug(`USER: ${os.userInfo().username}`)
    this.log.debug(`DIR: ${cwd}`)
    this.log.debug(`CMD: ${command.join(' ')}`)

    await new Promise((resolve, reject) => {
      const term = spawn(command.shift() ?? '', command, {
        cwd,
        env
      })

      // send stdout data from the process to all clients
      term.stdout.on('data', (data) => {
        this.log.debug('stdout: ' + data)
      })
      term.stderr.on('data', (data) => {
        this.log.debug('stderr: ' + data)
      })

      // send an error message to the client if the command does not exit with code 0
      term.on('exit', (code: any) => {
        if (code === 0) {
          clearTimeout(timeoutTimer)
          this.log.debug('Operation succeeded!')
          resolve(null)
        } else {
          clearTimeout(timeoutTimer)
          reject('Operation failed. Please review log for details.')
        }
      })

      // if the command spends to long trying to execute kill it after 5 minutes
      timeoutTimer = setTimeout(() => {
        term.kill('SIGTERM')
      }, 300000)
    })
  }
}
