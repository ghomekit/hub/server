import DeviceInterface from "./DeviceInterface"

export default interface DriverInterface {
  name: string
  devices: { [key: string]: DeviceInterface }
  stateHistorySchema?: { [key: string]: { type: string, title: string } }

  init (): Promise<void>
  afterDriverInit ?(): void | undefined
  getDevice (id: string): DeviceInterface | undefined
}
