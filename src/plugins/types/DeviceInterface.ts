import DriverInterface from "./DriverInterface"

export default interface DeviceInterface {
  id: string
  name: string
  driver: DriverInterface
  stateHistorySchema?: { [key: string]: { type: string, title: string } }

  runCommand (command: string): void
  getAttribute (name: string): string | number | null
}
