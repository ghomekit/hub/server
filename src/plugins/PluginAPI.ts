import Logger from '~/logger/Logger'
import Server from '~/Server'
import StateHistoryLogger from '~/plugins/StateHistoryLogger'
import Event, { EventValue } from '~/automations/Event'

import type DeviceInterface from '~/plugins/types/DeviceInterface'
import type DriverInterface from '~/plugins/types/DriverInterface'


export default class PluginAPI {
  private log: Logger = Logger.internal
  private server: Server

  constructor (server: Server) {
    this.server = server
  }


  public registerDriver (driver: DriverInterface): void {
    this.server.pluginService.registerDriver(driver)
  }

  public getDriver (name: string): DriverInterface {
    return this.server.pluginService.getDriver(name)
  }

  public getDriverNames (): string[] {
    return this.server.pluginService.getDriverNames()
  }

  public getLogger (driver: string): Logger {
    return Logger.withPrefix(driver)
  }

  public getStateHistoryLogger (driver: string): StateHistoryLogger {
    return new StateHistoryLogger(this.server.db, driver)
  }

  async getConfig (name: string, driver: string, device?: string): Promise<string | undefined> {
    // eslint-disable-next-line no-multi-str
    const [rows]: any[] = await this.server.db.get().execute('\
      SELECT `value`\
      FROM `general_config`\
      WHERE\
        `name` = ? AND\
        (`driver` = ? OR `driver` is NULL) AND\
        (`device` = ? OR `device` is NULL)\
      ORDER BY `driver` DESC, `device` DESC, `name` DESC\
    ', [ name, driver, device ?? null ])

    // console.log(rows)

    if (rows.length === 0) {
      this.log.debug(`No config for ${name}, ${driver}, ${device}`)
      return undefined
    }

    const output = rows[0].value

    this.log.debug(`Getting config for ${name}, ${driver}, ${device}: ${output}`)

    return output
  }

  async setConfig (name: string, value: string, driver: string, device?: string): Promise<void> {
    // eslint-disable-next-line no-multi-str
    const output: any = await this.server.db.get().execute('\
      UPDATE `general_config` SET `value` = ? WHERE `name` = ? AND `driver` = ? AND `device` = ?\
    ', [value, name, driver ?? null, device ?? null])

    if (output[0].affectedRows === 0) {
      // eslint-disable-next-line no-multi-str
      this.server.db.get().execute('\
        INSERT INTO `general_config` (`name`, `value`, `driver`, `device`) VALUES (?, ?, ?, ?)\
      ', [name, value, driver ?? null, device ?? null])
    }

    this.log.debug(`Setting config for ${name}, ${driver}, ${device}: ${value}`)
  }

  public emitEvent (name: string, device: DeviceInterface, newValue?: EventValue, oldValue?: EventValue) {
    if (!this.server.automationManager) {
      this.log.warn('Event ' + name + ' emitted too soon')
      return
    }

    let valueStr = ''

    if (newValue !== undefined) {
      if (oldValue !== undefined) {
        valueStr = ' (' + oldValue + '->' + newValue + ')'
      } else {
        valueStr = ' (' + newValue + ')'
      }
    }

    this.log.debug('Event \'' + name + '\' from ' + device.driver.name + ' ' + device.id + valueStr)

    this.server.automationManager.handleEvent(new Event(device, name, newValue, oldValue))
  }
}
