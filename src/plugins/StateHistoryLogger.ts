import MySQLStorage from '~/database/MySQLStorage'

export default class StateHistoryLogger {
  private db: MySQLStorage
  private driver: String

  constructor (db: MySQLStorage, driver: string) {
    this.db = db
    this.driver = driver
  }

  public async log (name: string, value: string, device?: string): Promise<void> {
    await this.db.get().execute(
      'INSERT INTO `device_state_history` (`driver`, `device`, `name`, `value`) VALUES (?, ?, ?, ?)',
      [this.driver, device, name, value]
    )
  }
}
