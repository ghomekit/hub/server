import path from 'path'
import fs from 'fs'

import Logger from '~/logger/Logger'
import PluginApi from '~/plugins/PluginAPI'
import Server from '~/Server'

import DriverInterface from '~/plugins/types/DriverInterface'


// incomplete type for package.json (just stuff we use here)
export interface PackageJson {
  name: string
  version: string
  keywords?: string[]

  main?: string;

  // engines?: Record<string, string>
  // dependencies?: Record<string, string>
  // devDependencies?: Record<string, string>
  // peerDependencies?: Record<string, string>
}

export default class PluginService {
  private static readonly PLUGIN_IDENTIFIER_PATTERN = /^((@[\w-]*)\/)?(ghome-[\w-]*)$/

  private log: Logger = Logger.internal
  private server: Server

  private searchPaths: string[] = []
  private plugins: string[] = []
  private drivers: { [key: string]: DriverInterface } = {}

  private pluginApi: PluginApi

  constructor (server: Server) {
    this.server = server
    this.pluginApi = new PluginApi(this.server)
  }


  public static isQualifiedPluginIdentifier (identifier: string): boolean {
    return PluginService.PLUGIN_IDENTIFIER_PATTERN.test(identifier)
  }

  private static loadPackageJson (pluginPath: string): PackageJson {
    const packageJsonPath = path.join(pluginPath, 'package.json')
    let packageJson: PackageJson

    if (!fs.existsSync(packageJsonPath)) {
      throw new Error(`Plugin ${pluginPath} does not contain a package.json.`)
    }

    try {
      packageJson = JSON.parse(fs.readFileSync(packageJsonPath, { encoding: 'utf8' })) // attempt to parse package.json
    } catch (err) {
      throw new Error(`Plugin ${pluginPath} contains an invalid package.json. Error: ${err}`)
    }

    if (!packageJson.name || !PluginService.isQualifiedPluginIdentifier(packageJson.name)) {
      throw new Error(`Plugin ${pluginPath} does not have a package name that begins with 'ghome-' or '@scope/ghome-.`)
    }

    // verify that it's tagged with the correct keyword
    if (!packageJson.keywords || !packageJson.keywords.includes('ghome-plugin')) {
      throw new Error(`Plugin ${pluginPath} package.json does not contain the keyword 'ghome-plugin'.`)
    }

    return packageJson
  }


  // This is a test that gets the plugin list from the database
  private async loadInstalledPluginsDB () {
    //   // Get plugins from database
    //   const [rows]: any[] = await this.db.get().query('\
    //     SELECT `name`\
    //     FROM `plugins`\
    //   ')

    //   for (const row of rows) {
    //     const name = row.name

    //     this.log.info('Loading plugin ' + name)

    //     // Check if plugin has a valid name
    //     if (!PluginService.isQualifiedPluginIdentifier(name)) {
    //       this.log.error(`Plugin ${name} has an invalid name`)
    //       continue
    //     }

    //     const plugin = await this.lpm.install(name)
    //     const pluginOutput = this.lpm.require(name)

    //     const packageJson = PluginService.loadPackageJson(plugin.location)

    //     let pluginInitializer = null

    //     if (typeof pluginOutput === 'function') {
    //       pluginInitializer = pluginOutput
    //     } else if (plugin && typeof pluginOutput.default === 'function') {
    //       pluginInitializer = pluginOutput.default
    //     } else {
    //       throw new Error(`Plugin ${plugin.name} does not export a initializer function from main.`)
    //     }

    //     pluginInitializer(this)

    //     this.plugins.push(name)
    //   }

    //   if (this.plugins.length) {
    //     this.log.info('Loaded ' + this.plugins.length + ' plugins')
    //   } else {
    //     this.log.warn('No plugins found. See the README for information on installing plugins.')
    //   }

    //   // Init each driver and get array of promises
    //   const driverPromises = Object.values(this.drivers).map(driver => {
    //     return driver.init()
    //   })

    //   // Wait for all drivers to be ready
    //   Promise.all(driverPromises)

    //   this.log.info('All drivers initialized')

    //   // Send a afterInit event to each driver
    //   Object.values(this.drivers).forEach(driver => {
    //     // Check if driver does not have afterDriverInit function
    //     if (typeof driver.afterDriverInit !== 'function') {
    //       return
    //     }

    //     driver.afterDriverInit()
    //   })
  }

  public loadInstalledPlugins (): void {
    this.loadDefaultPaths()

    // search for plugins among all known paths
    this.searchPaths.forEach(searchPath => {
      if (!fs.existsSync(searchPath)) {
        // just because this path is in require.main.paths doesn't mean it necessarily exists!
        return
      }

      // does this path point inside a single plugin and not a directory containing plugins?
      if (fs.existsSync(path.join(searchPath, 'package.json'))) {
        try {
          this.loadPlugin(searchPath)
        } catch (e: any) {
          this.log.warn(e.message)
        }
      } else {
        // read through each directory in this node_modules folder
        const relativePluginPaths = fs.readdirSync(searchPath) // search for directories only
          .filter(relativePath => {
            try {
              return fs.statSync(path.resolve(searchPath, relativePath)).isDirectory()
            } catch (e: any) {
              this.log.debug(`Ignoring path ${path.resolve(searchPath, relativePath)} - ${e.message}`)
              return false
            }
          })

        // expand out @scoped plugins
        relativePluginPaths.slice()
          .filter(path => path.charAt(0) === '@') // is it a scope directory?
          .forEach(scopeDirectory => {
            // remove scopeDirectory from the path list
            const index = relativePluginPaths.indexOf(scopeDirectory)
            relativePluginPaths.splice(index, 1)

            const absolutePath = path.join(searchPath, scopeDirectory)
            fs.readdirSync(absolutePath)
              .filter(name => PluginService.isQualifiedPluginIdentifier(name))
              .filter(name => {
                try {
                  return fs.statSync(path.resolve(absolutePath, name)).isDirectory()
                } catch (e: any) {
                  this.log.debug(`Ignoring path ${path.resolve(absolutePath, name)} - ${e.message}`)
                  return false
                }
              })
              .forEach(name => relativePluginPaths.push(scopeDirectory + '/' + name))
          })

        relativePluginPaths
          .filter(pluginIdentifier => {
            return PluginService.isQualifiedPluginIdentifier(pluginIdentifier) // needs to be a valid plugin name
          })
          .forEach(pluginIdentifier => {
            try {
              const absolutePath = path.resolve(searchPath, pluginIdentifier)
              this.loadPlugin(absolutePath)
            } catch (e: any) {
              this.log.warn(e.message)
            }
          })
      }
    })

    if (!this.plugins.length) {
      this.log.warn('No plugins found. See the README for information on installing plugins.')
      return
    }

    this.log.info('All plugins loaded')

    // Init each driver and get array of promises
    const driverPromises = Object.values(this.drivers).map(driver => {
      return driver.init()
    })

    // Wait for all drivers to be ready
    Promise.all(driverPromises)

    this.log.info('All drivers initialized')

    // Send a afterInit event to each driver
    Object.values(this.drivers).forEach(driver => {
      // Check if driver does not have afterDriverInit function
      if (typeof driver.afterDriverInit !== 'function') {
        return
      }

      driver.afterDriverInit()
    })
  }

  private loadPlugin (absolutePath: string) {
    this.log.debug('Found ' + absolutePath)

    const packageJson: PackageJson = PluginService.loadPackageJson(absolutePath)

    if (this.plugins.includes(packageJson.name)) {
      throw new Error(`Plugin ${absolutePath} already loaded`)
    }

    this.log.info('Loading plugin ' + packageJson.name)

    if (!packageJson.main) {
      throw new Error(`Plugin ${packageJson.name} does not have a main file`)
    }

    const plugin = require(path.join(absolutePath, packageJson.main))

    this.plugins.push(packageJson.name)

    let pluginInitializer = null

    if (typeof plugin === 'function') {
      pluginInitializer = plugin
    } else if (plugin && typeof plugin.default === 'function') {
      pluginInitializer = plugin.default
    } else {
      throw new Error(`Plugin ${packageJson.name} does not export a initializer function from main.`)
    }

    pluginInitializer(this.pluginApi)
  }

  private loadDefaultPaths (): void {
    if (require.main) {
      // add the paths used by require()
      // console.log(require.main)
      require.main.paths.forEach(path => this.searchPaths.push(path))
    }
  }


  public registerDriver (driver: DriverInterface): void {
    // Receives driver register request from Drivers
    this.drivers[driver.name] = driver
  }

  public getDriver (name: string): DriverInterface {
    return this.drivers[name]
  }

  public getDriverNames (): string[] {
    return Object.keys(this.drivers)
  }
}
