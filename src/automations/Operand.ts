import * as Schedule from 'node-schedule'
import PluginService from '~/plugins/PluginService'
import TimePattern from './TimePattern'
import Event, { EventValue } from './Event'

export type OperandInferred = string | number | (string | number)[]
export type OperandResult = EventValue | TimePattern | Date | null

export default class Operand {
  type: string
  value: string

  // TODO: constructor (device: DeviceInterface, property: string)
  constructor (type: string, value: string)
  constructor (value: OperandInferred)
  constructor (...args: any[]) {
    if (args.length === 1) {
      let type = null
      if (typeof args[0] === 'number') type = 'number'
      if (typeof args[0] === 'string') type = 'string'
      if (args[0] instanceof Array) type = 'array'

      if (!type) {
        throw new Error('Cannot infer Operand type')
      }

      this.type = type
      this.value = args[0]
    } else if (args.length === 2) {
      this.type = args[0]
      this.value = args[1]
    } else {
      throw new Error('Invalid Operand constructor')
    }
  }


  public evaluate (pluginService: PluginService, event?: Event): EventValue | TimePattern | Date | null {
    switch(this.type) {
      case 'number':
        return Number(this.value)
      case 'string':
        return this.value
      case 'timePattern':
        return new TimePattern(this.value)
      case 'device':
        const parts = this.value.split('.')

        if (parts.length !== 3) return null

        const value = pluginService.getDriver(parts[0])?.getDevice(parts[1])?.getAttribute(parts[2])

        if (!value) return null

        return value
      case 'array':
        return this.value.split(',')
      case 'event':
        if (!event) return null
        if (this.value === 'value' && event.value) return event.value
        if (this.value === 'oldValue' && event.oldValue) return event.oldValue
        if (this.value === 'timestamp') return event.timestamp
        return null
    }

    return null
  }
}
