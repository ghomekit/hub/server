import Condition from './Condition'
import Action from './Action'
import Event from './Event'

export default class Automation {
  id: number
  name?: string
  conditions?: Condition[]
  actions?: Action[]

  constructor (id: number, name?: string, conditions?: Condition[], actions?: Action[]) {
    this.id = id

    this.name = name ?? 'Unnamed'
    this.conditions = conditions ?? []
    this.actions = actions ?? []
  }

  public checkConditions (event?: Event): boolean {
    if (this.conditions === undefined) {
      return true
    }

    for (const condition of this.conditions) {
      if (!condition.evaluate(event)) return false
    }

    return true
  }

  public runActions (): void {
    if (this.actions === undefined) return

    for (const action of this.actions) {
      action.run()
    }
  }

  public run (event?: Event): boolean {
    if (!this.checkConditions(event)) return false

    this.runActions()

    return true
  }

  public setConditions (conditions: Condition[]): void {
    this.conditions = conditions
  }

  public setActions (actions: Action[]): void {
    this.actions = actions
  }
}
