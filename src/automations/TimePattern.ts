import Logger from '~/logger/Logger'

type NumberRange = {
  start?: number
  end?: number
}

export default class TimePattern {
  private logger: Logger = Logger.internal

  public year: NumberRange = {
    start: undefined,
    end: undefined
  }

  public month: NumberRange = {
    start: undefined,
    end: undefined
  }

  public day: NumberRange = {
    start: undefined,
    end: undefined
  }

  public weekday: NumberRange = {
    start: undefined,
    end: undefined
  }

  public hour: NumberRange = {
    start: undefined,
    end: undefined
  }

  public minute: NumberRange = {
    start: undefined,
    end: undefined
  }

  public second: NumberRange = {
    start: undefined,
    end: undefined
  }

  constructor (timePattern?: string) {
    if (!timePattern) {
      return
    }

    if (!TimePattern.isTimePattern(timePattern)) {
      this.logger.error('Invalid time pattern')
      return
    }

    const parts = timePattern.split(' ')

    let subpart = parts[0].split('-')
    if (subpart.length === 2) {
      this.year = {
        start: parseInt(subpart[0]),
        end: parseInt(subpart[1])
      }
    } else if (subpart.length === 1) {
      if (!isNaN(parseInt(subpart[0]))) {
        this.year = {
          start: parseInt(subpart[0]),
          end: parseInt(subpart[0])
        }
      }
    }

    subpart = parts[1].split('-')
    if (subpart.length === 2) {
      this.month = {
        start: parseInt(subpart[0]) ?? undefined,
        end: parseInt(subpart[1])
      }
    } else if (subpart.length === 1) {
      if (!isNaN(parseInt(subpart[0]))) {
        this.month = {
          start: parseInt(subpart[0]),
          end: parseInt(subpart[0])
        }
      }
    }

    subpart = parts[2].split('-')
    if (subpart.length === 2) {
      this.day = {
        start: parseInt(subpart[0]),
        end: parseInt(subpart[1])
      }
    } else if (subpart.length === 1) {
      if (!isNaN(parseInt(subpart[0]))) {
        this.day = {
          start: parseInt(subpart[0]),
          end: parseInt(subpart[0])
        }
      }
    }

    subpart = parts[3].split('-')
    if (subpart.length === 2) {
      this.weekday = {
        start: parseInt(subpart[0]),
        end: parseInt(subpart[1])
      }
    } else if (subpart.length === 1) {
      if (!isNaN(parseInt(subpart[0]))) {
        this.weekday = {
          start: parseInt(subpart[0]),
          end: parseInt(subpart[0])
        }
      }
    }

    subpart = parts[4].split('-')
    if (subpart.length === 2) {
      this.hour = {
        start: parseInt(subpart[0]),
        end: parseInt(subpart[1])
      }
    } else if (subpart.length === 1) {
      if (!isNaN(parseInt(subpart[0]))) {
        this.hour = {
          start: parseInt(subpart[0]),
          end: parseInt(subpart[0])
        }
      }
    }

    subpart = parts[5].split('-')
    if (subpart.length === 2) {
      this.minute = {
        start: parseInt(subpart[0]),
        end: parseInt(subpart[1])
      }
    } else if (subpart.length === 1) {
      if (!isNaN(parseInt(subpart[0]))) {
        this.minute = {
          start: parseInt(subpart[0]),
          end: parseInt(subpart[0])
        }
      }
    }

    subpart = parts[6].split('-')
    if (subpart.length === 2) {
      this.second = {
        start: parseInt(subpart[0]),
        end: parseInt(subpart[1])
      }
    } else if (subpart.length === 1) {
      if (!isNaN(parseInt(subpart[0]))) {
        this.second = {
          start: parseInt(subpart[0]),
          end: parseInt(subpart[0])
        }
      }
    }
  }

  public static isTimePattern (timePattern: string): boolean {
    return /^(([0-9]*-[0-9]*)|[0-9]+|\*)( (([0-9]*-[0-9]*)|[0-9]+|\*)){6}$/.test(timePattern)
  }

  public checkPattern (time?: Date): boolean {
    time = time || new Date()

    const year = time.getFullYear()
    const month = time.getMonth() + 1
    const day = time.getDate()
    const weekday = time.getDay()
    const hour = time.getHours()
    const minute = time.getMinutes()
    const second = time.getSeconds()

    if (this.year.start && year < this.year.start) {
      return false
    }
    if (this.year.end && year > this.year.end) {
      return false
    }
    if (this.month.start && month < this.month.start) {
      return false
    }
    if (this.month.end && month > this.month.end) {
      return false
    }
    if (this.day.start && day < this.day.start) {
      return false
    }
    if (this.day.end && day > this.day.end) {
      return false
    }
    if (this.weekday.start && weekday < this.weekday.start) {
      return false
    }
    if (this.weekday.end && weekday > this.weekday.end) {
      return false
    }
    if (this.hour.start && hour < this.hour.start) {
      return false
    }
    if (this.hour.end && hour > this.hour.end) {
      return false
    }
    if (this.minute.start && minute < this.minute.start) {
      return false
    }
    if (this.minute.end && minute > this.minute.end) {
      return false
    }
    if (this.second.start && second < this.second.start) {
      return false
    }
    if (this.second.end && second > this.second.end) {
      return false
    }

    return true
  }

  public includes (time?: Date): boolean {
    return this.checkPattern(time)
  }
}
