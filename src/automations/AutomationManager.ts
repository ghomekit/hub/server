import Logger from '~/logger/Logger'
import Server from '~/Server'

import Automation from './Automation'
import Condition from './Condition'
import Operand from './Operand'
import Event, { EventValue } from './Event'
import Action from './Action'
import * as Schedule from 'node-schedule'

import AutomationRepository from '~/repositories/Automation'

export default class AutomationManager {
  private logger: Logger = Logger.internal
  private server: Server

  eventAutomations: { [key: string]: { [key: string]: { [key: string]: Automation[] } } } = {}
  timeAutomations: Schedule.Job[] = []

  constructor (server: Server) {
    this.server = server
  }


  async loadAutomations () {
    this.logger.debug('Loading automations...')

    const automationRepository = new AutomationRepository(this.server.db)

    const automations = await automationRepository.list()

    for (const automation of automations) {
      this.logger.debug('Loading automation ' + automation.name + ' (' + automation.id + ')')

      const automationId = automation.id ?? 0

      const eventTriggers = await automationRepository.listEventTriggers(automationId)
      const timeTriggers = await automationRepository.listTimeTriggers(automationId)
      const conditions = await automationRepository.listConditions(automationId)
      const actions = await automationRepository.listActions(automationId)

      const automationObj = new Automation(
        automationId,
        automation.name,
        conditions.map(c => new Condition(
            this.server.pluginService,
            new Operand(c.left_operand_type, c.left_operand_value),
            c.operator,
            new Operand(c.right_operand_type, c.right_operand_value)
          )),
        actions.map(a => new Action(this.server.pluginService, a.target, a.command, a.timeout))
      )

      for (const eventTrigger of eventTriggers) {
        this.eventAutomations[eventTrigger.driver] = this.eventAutomations[eventTrigger.driver] ?? {}
        this.eventAutomations[eventTrigger.driver][eventTrigger.device] = this.eventAutomations[eventTrigger.driver][eventTrigger.device] ?? {}
        this.eventAutomations[eventTrigger.driver][eventTrigger.device][eventTrigger.event] = this.eventAutomations[eventTrigger.driver][eventTrigger.device][eventTrigger.event] ?? []

        this.eventAutomations[eventTrigger.driver][eventTrigger.device][eventTrigger.event].push(automationObj)
      }

      for (const timeTrigger of timeTriggers) {
        this.timeAutomations.push(
          Schedule.scheduleJob(timeTrigger.cron, () => {
            automationObj.run()
          })
        )
      }
    }

    this.logger.debug('Loaded ' + automations.length + ' automations')
  }

  unloadAutomations () {
    this.logger.debug('Unloading automations...')

    for (const job of this.timeAutomations) {
      job.cancel()
    }

    this.timeAutomations = []

    this.logger.debug('Unloaded automations')
  }

  async reloadAutomations () {
    this.unloadAutomations()
    await this.loadAutomations()
  }



  handleEvent (event: Event) {
    this.logger.debug('Received event ' + event.name + ' from ' + event.emitter.id + ' (' + event.emitter.driver.name + ')')

    if (this.eventAutomations[event.emitter.driver.name] === undefined) {
      // this.logger.debug('No automations for this driver')
      return
    }
    if (this.eventAutomations[event.emitter.driver.name][event.emitter.id] === undefined) {
      // this.logger.debug('No automations for this device')
      return
    }
    if (this.eventAutomations[event.emitter.driver.name][event.emitter.id][event.name] === undefined) {
      // this.logger.debug('No automations for this event')
      return
    }

    for (const automation of this.eventAutomations[event.emitter.driver.name][event.emitter.id][event.name]) {
      this.logger.debug('Checking conditions for automation ' + automation.name)
      const ran = automation.run(event)
      this.logger.debug('Automation ' + automation.name + ' (' + ran + ')')
    }
  }
}
