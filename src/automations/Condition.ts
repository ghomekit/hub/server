import PluginService from '~/plugins/PluginService'
import Logger from '~/logger/Logger'

import Event from './Event'
import Operand, { OperandInferred, OperandResult } from './Operand'
import TimePattern from './TimePattern'

export default class Condition {
  private pluginService: PluginService
  private logger: Logger = Logger.internal

  leftValue: Operand
  operator: string
  rightValue: Operand

  constructor (pluginService: PluginService, left: Operand | OperandInferred, operator: string, right: Operand | OperandInferred) {
    this.pluginService = pluginService

    this.leftValue = left instanceof Operand ? left : new Operand(left)
    this.operator = operator
    this.rightValue = right instanceof Operand ? right : new Operand(right)
  }


  public evaluate (event?: Event): boolean {
    const leftSide = this.leftValue.evaluate(this.pluginService, event)
    const rightSide = this.rightValue.evaluate(this.pluginService, event)

    this.logger.debug(
      '(' + typeof leftSide + ') ' + leftSide + ' ' + this.operator + ' (' + typeof rightSide + ') ' + rightSide + ''
    )

    switch (this.operator) {
      case '=':
      case '==':
        this.logger.debug((leftSide === rightSide).toString())
        return leftSide === rightSide
      case '>':
        if (leftSide === null || rightSide === null) return false
        if (!['number', 'string'].includes(typeof leftSide)) return false
        if (!['number', 'string'].includes(typeof rightSide)) return false

        return leftSide > rightSide
      case '>=':
        if (leftSide === null || rightSide === null) return false
        if (!['number', 'string'].includes(typeof leftSide)) return false
        if (!['number', 'string'].includes(typeof rightSide)) return false

        return leftSide >= rightSide
      case '<':
        if (leftSide === null || rightSide === null) return false
        if (!['number', 'string'].includes(typeof leftSide)) return false
        if (!['number', 'string'].includes(typeof rightSide)) return false

        return leftSide < rightSide
      case '<=':
        if (leftSide === null || rightSide === null) return false
        if (!['number', 'string'].includes(typeof leftSide)) return false
        if (!['number', 'string'].includes(typeof rightSide)) return false

        return leftSide <= rightSide
      case '!=':
      case '<>':
        this.logger.debug((leftSide !== rightSide).toString())
        return leftSide !== rightSide
      case 'in':
        return this.evaluateIn(leftSide, rightSide)
      case 'not in':
        return !this.evaluateIn(leftSide, rightSide)
    }

    this.logger.error('Invalid operator ' + this.operator)
    return false
  }

  private evaluateIn(leftSide: OperandResult, rightSide: OperandResult): boolean {
    // number in array
    // string in array
    // array in array
    // string in string
    // Date(string) in timePattern

    if (!leftSide) return false
    if (!rightSide) return false

    if (rightSide instanceof TimePattern) {
      if (!(leftSide instanceof Date)) return false

      let date = null
      if (leftSide instanceof String) date = new Date(leftSide)
      if (leftSide instanceof Date) date = leftSide

      if (!date) return false

      // Check if leftSide is in rightSide
      if (!rightSide.includes(date)) return false

      return true
    }

    if (rightSide instanceof Array) {
      if (leftSide instanceof Date) return false
      if (leftSide instanceof TimePattern) return false

      if (leftSide instanceof Array) {
        return leftSide.reduce((accumulator, current) => {
          return accumulator && rightSide.includes(current)
        }, true)
      }

      return rightSide.includes(leftSide)
    }

    if (typeof rightSide === 'string') {
      if (leftSide instanceof Date) return false
      if (leftSide instanceof TimePattern) return false
      if (leftSide instanceof Array) return false

      return rightSide.includes(leftSide.toString())
    }

    return false
  }
}
