import DeviceInterface from '~/plugins/types/DeviceInterface'

export type EventValue = string | number | (string | number)[]

export default class Event {
  emitter: DeviceInterface
  name: string
  value?: EventValue
  oldValue?: EventValue
  timestamp = new Date()

  constructor (emitter: DeviceInterface, name: string, value?: EventValue, oldValue?: EventValue) {
    this.emitter = emitter
    this.name = name
    this.value = value
    this.oldValue = oldValue
  }
}
