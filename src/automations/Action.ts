import PluginService from '~/plugins/PluginService'
import DriverInterface from '~/plugins/types/DriverInterface'
import Logger from '~/logger/Logger'

export default class Action {
  pluginService: PluginService
  private logger: Logger = Logger.internal

  target: string
  command: string

  timeOut: number = 0

  constructor (pluginService: PluginService, target: string, command: string, timeOut?: number) {
    this.pluginService = pluginService

    this.target = target
    this.command = command

    this.timeOut = timeOut ?? 0
  }

  run (): void {
    const parts = this.target.split('.')

    setTimeout(() => {
      this.logger.debug('Running \'' + this.command + '\' at \'' + this.target + '\' (' + this.timeOut + ')')

      const driver: DriverInterface = this.pluginService.getDriver(parts[0])

      if (!driver) {
        return
      }

      const device = driver.getDevice(parts[1])

      if (!device) {
        return
      }

      device.runCommand(this.command)
    }, this.timeOut)
  }
}
