import express from 'express'
import Server from '~/Server'
import Logger from '~/logger/Logger'
import PluginTest from '~/plugins/PluginTest'

export default class HTTPService {
  private log: Logger = Logger.internal
  private app: express.Application
  private port: number = 3030
  private server: Server

  constructor (server: Server) {
    this.server = server

    this.app = express()
    this.app.use(express.json())

    this.app.post('/plugins', async (req: any, res: any) => {
      this.log.debug('POST /plugins')

      const ps = new PluginTest()
      await ps.runNpmCommand(
        [...ps.getNpmPath(), 'install', req.body.name, '-g'],
        process.cwd()
      )

      res.send('ok')
    })

    this.app.delete('/plugins', async (req: any, res: any) => {
      this.log.debug('POST /plugins')

      const ps = new PluginTest()
      await ps.runNpmCommand(
        [...ps.getNpmPath(), 'uninstall', req.body.name],
        process.cwd()
      )

      res.send('ok')
    })


  }

  public start() {
    this.app.listen(this.port, () => {
      this.log.info(`HTTP Service listening on port ${this.port}`)
    })
  }
}
