export interface MySQLConfig {
  host?: string,
  port?: number,
  user?: string,
  password?: string,
  database?: string,
  connectionLimit?: number,
}

export default interface ServerConfig {
  database: MySQLConfig
}
