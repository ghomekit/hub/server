import ServerConfig from '~/ServerConfig'

import MySQLStorage from '~/database/MySQLStorage'
import Logger from '~/logger/Logger'

import HTTPService from '~/http/HTTPService'
import PluginService from '~/plugins/PluginService'
import AutomationManager from '~/automations/AutomationManager'


export default class Server {
  private logger: Logger = Logger.internal
  private config: ServerConfig

  db: MySQLStorage
  httpService: HTTPService
  automationManager: AutomationManager
  pluginService: PluginService

  constructor (config: ServerConfig) {
    this.config = config

    this.db = new MySQLStorage()

    this.httpService = new HTTPService(this)
    this.automationManager = new AutomationManager(this)
    this.pluginService = new PluginService(this)

    Logger.setDebugEnabled()
  }

  async start () {
    // this.printBanner()
    this.logger.info('Welcome to gHome server!')

    try {
      await this.db.connect(this.config.database)
    } catch (err: any) {
      this.logger.error('Failed to connect to database: ' + err.message)
      process.exit(1)
    }

    this.logger.info('Connected to database')

    this.pluginService.loadInstalledPlugins()
    await this.automationManager.loadAutomations()
    this.httpService.start()
  }

  printBanner () {
    this.logger.info('        _    _                      ')
    this.logger.info('       | |  | |                     ')
    this.logger.info('   __ _| |__| | ___  _ __ ___   ___ ')
    this.logger.info('  / _` |  __  |/ _ \\| \'_ ` _ \\ / _ \\')
    this.logger.info(' | (_| | |  | | (_) | | | | | |  __/')
    this.logger.info('  \\__, |_|  |_|\\___/|_| |_| |_|\\___|')
    this.logger.info('   __/ |                            ')
    this.logger.info('  |___/                             ')
    this.logger.info('')
  }
}
