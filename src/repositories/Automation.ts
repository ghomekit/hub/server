import MySQLStorage from '~/database/MySQLStorage'
import { RowDataPacket, OkPacket } from 'mysql2'

export interface AutomationModel extends RowDataPacket {
  id?: number
  name: string
  enabled: boolean
}

export interface AutomationEventTriggerModel extends RowDataPacket {
  id?: number
  automation: number
  driver: string
  device: string
  event: string
}

export interface AutomationTimeTriggerModel extends RowDataPacket {
  id?: number
  automation: number
  cron: string
}

export interface AutomationConditionModel extends RowDataPacket {
  id?: number
  automation: number
  left_operand_type: string
  left_operand_value: string
  operator: string
  right_operand_type: string
  right_operand_value: string
}

export interface AutomationActionModel extends RowDataPacket {
  id?: number
  automation: number
  target: string
  command: string
  timeout: number
}


export default class AutomationRepository {
  private db: MySQLStorage

  constructor(db: MySQLStorage) {
    this.db = db
  }

  async list(): Promise<AutomationModel[]> {
    const [rows, fields] = await this.db.get().query<AutomationModel[]>("SELECT * FROM automations")

    return rows
  }

  async get(id: number): Promise<AutomationModel> {
    const [rows, fields] = await this.db.get().query<AutomationModel[]>("SELECT * FROM automations WHERE id = ?", [id])

    return rows[0]
  }

  async create(automation: AutomationModel): Promise<number> {
    const [rows, fields] = await this.db.get().query<OkPacket>("INSERT INTO automations SET ?", [automation])

    return rows.insertId
  }

  async update(automation: AutomationModel): Promise<void> {
    await this.db.get().query<OkPacket>("UPDATE automations SET ? WHERE id = ?", [automation, automation.id])
  }

  async delete(id: number): Promise<void> {
    await this.db.get().query<OkPacket>("DELETE FROM automations WHERE id = ?", [id])
  }



  async listEventTriggers(automationId: number): Promise<AutomationEventTriggerModel[]> {
    const [rows, fields] = await this.db.get().query<AutomationEventTriggerModel[]>("SELECT * FROM automation_event_triggers WHERE automation = ?", [automationId])

    return rows
  }

  async listTimeTriggers(automationId: number): Promise<AutomationTimeTriggerModel[]> {
    const [rows, fields] = await this.db.get().query<AutomationTimeTriggerModel[]>("SELECT * FROM automation_time_triggers WHERE automation = ?", [automationId])

    return rows
  }

  async listConditions(automationId: number): Promise<AutomationConditionModel[]> {
    const [rows, fields] = await this.db.get().query<AutomationConditionModel[]>("SELECT * FROM automation_conditions WHERE automation = ?", [automationId])

    return rows
  }

  async listActions(automationId: number): Promise<AutomationActionModel[]> {
    const [rows, fields] = await this.db.get().query<AutomationActionModel[]>("SELECT * FROM automation_actions WHERE automation = ?", [automationId])

    return rows
  }
}
