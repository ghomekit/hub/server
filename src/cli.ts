// import os from 'os'
// import path from 'path'
import 'dotenv/config'

// import cron from "node-cron"

import Server from '~/Server'

// eslint-disable-next-line no-unused-vars
const server = new Server({
  // dir: path.join(os.homedir(), '.ghome'),
  database: {
    host: process.env.DB_HOST,
    port: Number(process.env.DB_PORT),
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    connectionLimit: Number(process.env.DB_CONNECTION_LIMIT)
  }
})

server.start()
