import { createConnection, Connection } from 'mysql2/promise'

import { MySQLConfig } from '~/ServerConfig'

export default class MySQLStorage {
  db?: Connection

  async connect (config: MySQLConfig) {
    // this.db = createPool({
    //   host: config.host ?? 'localhost',
    //   port: config.port ?? 3306,
    //   user: config.user ?? 'root',
    //   password: config.password ?? '',
    //   database: config.database ?? 'ghome',
    //   connectionLimit: config.connlimit ?? 1
    // })

    this.db = await createConnection({
      host: config.host ?? 'localhost',
      port: config.port ?? 3306,
      user: config.user ?? 'root',
      password: config.password ?? '',
      database: config.database ?? 'ghome'
    })
  }

  get (): Connection {
    if (!this.db) {
      throw new Error('The db has not been initialized, call init({}) prior to get().')
    }

    return this.db
  }
}
