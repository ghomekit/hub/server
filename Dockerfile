FROM node:lts-alpine as builder

WORKDIR /app

COPY package.json .
COPY yarn.lock .

RUN yarn --non-interactive

COPY . .

RUN yarn build


FROM node:lts-alpine

WORKDIR /app

COPY package.json .
COPY yarn.lock .

RUN yarn --production --non-interactive

COPY --from=builder ./app/build ./build/

ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
ENV PATH=$PATH:/home/node/.npm-global/bin

EXPOSE 3030

CMD ["node", "build/cli.js"]
